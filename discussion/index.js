// alert('hi')

let count = 5;

// While Loop

/*
	- A while loop takes in an expression/condition
	- Expressions are any unit of code that can be evaluated to a value
	- If the condition evaluates to true, the statements inside the code block will be executed
	- A statement is a command that the programmer gives to the computer
	- A loop will iterate a certain number of times until an expression/condition is met
	- "Iteration" is the term given to the repetition of statements

*/
	// Syntax: 
		// while(expression/condition){
		// 	statement/ block of codes
		// }

// While the value of count is not equal to 0
while(count !== 0){

   	// The current value of count is printed out
	console.log("While:" + count)


	// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0

	// Forgetting to include this in loops will make our applications run an infinite loop which will eventually crash our devices
	count--
};

// while(5 !== 0) {
// 	console.log(While: + 5)
// 	count-- (5-1)
// }

// Do-While Loop

/*
	
A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

*/

/*Syntax:
		do {
			statement
		} while(expression/condition)

*/

// The "Number" function works similarly to the "parseInt" function
// The "prompt" function creates a pop-up message in the browser that can be used to gather user input
let number = Number(prompt('Give me a number'))

do {

	 // The current value of number is printed out
	console.log("Do While:" + number)

	 // Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
		    // number = number + 1
	number += 1

// Providing a number of 10 or greater will run the code block once and will stop the loop	
} while(number < 10)

/* Syntax:
do {
	console.log(Do While: + 11)
	11 += 1 (result- 12)
} while(number < 10) 

*/

// For Loops
// 3 parts of for loops
	// initialization- value that will track the progression of the loop.
	// expression/ condition- that will be evaluated which will determine whether the loop will run one more time.
	// iteration- indicates how to advance the loop.
/*	
	Syntax:
	for(initialization; expression/condition; iteration){
		statement/ code block
	}

*/

for(let count = 0; count <= 20; count++){

	// The current value of count is printed out
	console.log(count);
};

/*	for(start; should reach false; add 1){
	
}
	for(0; 0 <= 20; 0 + 1= 1){
		console.log(0)
	}

*/

let myString = "alex";
// String is an array of characters
// let myString = [a, l, e, x]

// result- 4 (number of characters/ elements)
console.log(myString.length);

// Accessing elements of a string
// Individual characters of a string may be accessed using it's index number
// The first character in a string corresponds to the number 0, the next is 1 up to the nth number
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

//total index= length - 1



// Will create a loop that will print out the individual letters of the myString variable
for(let x = 0; x < myString.length; x++){

	// The current value of myString is printed out using it's index value
	console.log(myString[x])
};




let myName = "archee"

for(let i = 0; i < myName.length; i++){

	// If the character of your name is a vowel letter, instead of displaying the character, display number "3"
	// The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"

	){

		// If the letter in the name is a vowel, it will print the number 3
		console.log(3);

	} else {

		// Print in the console all non-vowel characters in the name
		console.log(myName[i]);
	};
};

// Continue and Break

/*
    - The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
    - The "break" statement is used to terminate the current loop once a match has been found
*/


for(let count = 0; count <= 20; count++){


    // if remainder is equal to 0	
	if( count % 2 === 0){

		// Tells the code to continue to the next iteration of the loop
		// This ignores all statements located after the continue statement;
		continue;
	}

	// The current value of number is printed out if the remainder is not equal to 0
	console.log("Continue and Break:" + count)

	// If the current value of count is greater than 10
	if(count > 10){

		// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
		// number values after 10 will no longer be printed
		break;
	}
}
